

db.rooms.insertOne({
    name:"single",
    accomodates:2,
    price: 1000,
    description:"This room is located on the top floor of the hotel and has hot / cold air conditioned, a furnished balcony with sun loungers with swimming pool or mountain views and free WI FI    Room surface area: 17 m²    Bed options: 1 single bed (105 cm * 190 cm)  Room facilities: Pool or mountain views, terrace, telephone, flat screen TV with satellite channels, free safe, hot/cold air conditioned, desk,  parquet floor, wardrobe, mini fridge , hairdryer, free toiletries, gel / shampoo dispenser and bathtub.    Non smoking room.",
    rooms_available:3,
    isAvailable:false
});


db.rooms.insertMany(
    [
        {
        name:"double",
        accomodates:3,
        price: 2000,
        description:"This totally exterior room with furnished balcony and street views has hot/cold air-conditioned and free WI FI.Surface of the room: 22 m²Bed options: 3 single beds (90 cm * 90 cm) or 1 single bed (90 cm * 90 cm) and 1 extra-large double bed (180 cm * 190 cm)Room equipment: Furnished balcony with street views, telephone, flat screen TV with satellite channels, hot/cold air conditioned, desk, tiled floor, wardrobe with safe for hire, mini fridge, hairdryer, free toiletries, gel / shampoo dispenser and bathtub or shower.Non-smoking room",
        rooms_available:3,
        isAvailable:false
        },
        {
        name:"queen",
        accomodates:4,
        price: 4000,
        description:"Apartment completely renovated and soundproofed located on the first or second floor overlooking the garden, is composed of:• Bedroom 1: 1 large double bed (180 cm * 190 cm)• Bedroom 2: 2 single beds (90cm * 190cm)• Living room: 1 sofa bed (160 cm * 190 cm) Kitchen: refrigerator, microwave, coffee machine, kettle, cooking utensils, cookers, table service (plates, cutlery, glasses ...) 1 table and 6 chairs.Terrace with outdoor furniture.2 complete bathrooms with hair dryer, free toiletries, gel / shampoo dispenser, bathtub or shower, and 1 toilet with WC and sink.Room facilities: Furnished balcony, 1 flat screen TV in each room, 1 flat screen TV in the living room, Satellite channels, telephone, free WI-FI, free safe, air conditioning, seating area, cold air conditioning / hot, sofa bed, soundproofing, wooden / parquet floor, cupboards. Non smoking room.",
        rooms_available:15,
        isAvailable:false
        }
    ]
);

db.rooms.find(
    {
    "name":"double"
    }
);